import axios from 'axios'
import store from '@/store'

axios.defaults.baseURL = 'http://testapi.doitserver.in.ua/api'

export default {
  signUp (user) {
    return axios({
      method: 'POST',
      url: '/users',
      data: user
    })
  },
  signIn (user) {
    return axios({
      method: 'POST',
      url: '/auth',
      data: user
    })
  },
  getAllTasks (params) {
    return axios({
      method: 'GET',
      url: '/tasks',
      params: params,
      headers: {
        Authorization: `Bearer ${store.state.token}`
      }
    })
  },
  getTask (id) {
    return axios({
      method: 'GET',
      url: `/tasks/${id}`,
      headers: {
        Authorization: `Bearer ${store.state.token}`
      }
    })
  },
  createNewTask (task) {
    return axios({
      method: 'POST',
      url: '/tasks',
      headers: {
        Authorization: `Bearer ${store.state.token}`
      },
      data: task
    })
  },
  deleteTask (id) {
    return axios({
      method: 'DELETE',
      url: `/tasks/${id}`,
      headers: {
        Authorization: `Bearer ${store.state.token}`
      }
    })
  },
  saveEditedTask (id, task) {
    return axios({
      method: 'PUT',
      url: `/tasks/${id}`,
      headers: {
        Authorization: `Bearer ${store.state.token}`
      },
      data: task
    })
  }
}
