import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null
  },
  getters: {
    isSignedIn (state) {
      return state.token !== null
    }
  },
  mutations: {
    setToken (state, data) {
      state.token = data
    }
  },
  actions: {
    getTokenFromStorage ({ commit }) {
      let token = sessionStorage.getItem('token')

      if (token === null) {
        token = localStorage.getItem('token')
      }

      commit('setToken', token)
    }
  }
})
