import Vue from 'vue'

Vue.filter('shortDate', value => {
  const formatter = Intl.DateTimeFormat('en', {
    year: '2-digit',
    month: '2-digit',
    day: '2-digit'
  })
  return formatter.format(value * 1000)
})
